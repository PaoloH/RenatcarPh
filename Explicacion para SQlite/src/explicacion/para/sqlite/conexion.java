/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package explicacion.para.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC
 */
public class conexion {

    String url = "C:\\Users\\PC\\Desktop\\hola.db";
    Connection connect;

    public void conectar() {
        try {
            connect = DriverManager.getConnection("jdbc:sqlite:" + url);
            if (connect != null) {
                System.out.println("Conexion exitosa");
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo establecer una conexion");
        }

    }

    public void cerrarConexion() {
        try {
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
